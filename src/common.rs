use alloc::vec;
use alloc::vec::Vec;
use crate::DEFAULT_VERSION;
use crate::quantisation::{find_closest_index, palette};
use crate::types::{CBTFlags, CBTHeader, HeaderError};

#[derive(Debug)]
pub struct CBT {
    pub header: CBTHeader,
    pub frames: Vec<Vec<u8>>,
}

pub struct CBTReader {
    pub frames: Vec<Vec<u8>>,
}

#[derive(Debug)]
pub enum CBTError {
    HeaderError(HeaderError),
    TooShort,
    IndexOutOfBounds,
}

impl CBT {
    /// Creates a new CBT from the given bytes.
    /// Bytes should be the entire contents of a CBT file.
    pub fn new_from_bytes(bytes: &[u8]) -> Result<CBT, CBTError> {
        let header = CBTHeader::get_from_bytes(&bytes[0..16]);
        if let Err(e) = header {
            return Err(CBTError::HeaderError(e));
        }
        let header = header.unwrap();
        let version = header.version;
        let data = bytes[16..].to_vec();
        let is_alpha = header.flags.has_alpha_channel;
        if version == 1 {
            if data.len() < (header.width as usize * header.height as usize * header.frame_count as usize * { if is_alpha { 4 } else { 3 } }) {
                return Err(CBTError::TooShort);
            }
            let mut frames = Vec::new();
            for i in 0..header.frame_count as usize {
                let frame_size = header.width as usize * header.height as usize * { if is_alpha { 4 } else { 3 } };
                let frame_start = i * frame_size;
                let frame_end = frame_start + frame_size;
                let frame = &data[frame_start as usize..frame_end as usize];
                frames.push(frame.to_vec());
            }
            Ok(CBT {
                header,
                frames,
            })
        } else if version == 2 {
            if data.len() < (header.width as usize * header.height as usize * header.frame_count as usize * { if is_alpha { 2 } else { 1 } }) {
                return Err(CBTError::TooShort);
            }
            let mut frames = Vec::new();
            for i in 0..header.frame_count as usize {
                let frame_size = header.width as usize * header.height as usize * { if is_alpha { 2 } else { 1 } };
                let frame_start = i * frame_size;
                let frame_end = frame_start + frame_size;
                let frame = &data[frame_start as usize..frame_end as usize];
                frames.push(frame.to_vec());
            }
            Ok(CBT {
                header,
                frames,
            })
        } else {
            Err(CBTError::HeaderError(HeaderError::UnsupportedVersion))
        }
    }

    /// Creates a new CBTv1 with no data contained and a frame count of 0.
    /// Preferably use new_with_capacity instead, to save cpu time.
    pub fn new_empty_v1(width: u16, height: u16, with_alpha_channel: bool) -> CBT {
        let frame_count = 0;
        let flags = CBTFlags {
            has_alpha_channel: with_alpha_channel,
        };
        let header = CBTHeader {
            version: 1,
            width,
            height,
            frame_count,
            flags,
        };
        let frames = Vec::new();
        CBT {
            header,
            frames,
        }
    }

    /// Creates a new CBTv2 with no data contained and a frame count of 0.
    /// Preferably use new_with_capacity instead, to save cpu time.
    pub fn new_empty_v2(width: u16, height: u16, with_alpha_channel: bool) -> CBT {
        let frame_count = 0;
        let flags = CBTFlags {
            has_alpha_channel: with_alpha_channel,
        };
        let header = CBTHeader {
            version: 2,
            width,
            height,
            frame_count,
            flags,
        };
        let frames = Vec::new();
        CBT {
            header,
            frames,
        }
    }

    /// Creates a new CBTv1 with a pre-allocated frame count.
    /// Preferred over new_empty, to save cpu time.
    pub fn new_with_capacity_v1(width: u16, height: u16, frame_count: u16, with_alpha_channel: bool) -> CBT {
        let flags = CBTFlags {
            has_alpha_channel: with_alpha_channel,
        };
        let header = CBTHeader {
            version: 1,
            width,
            height,
            frame_count,
            flags,
        };
        let mut frames = Vec::new();
        let frame_size = header.width * header.height * {
            if header.flags.has_alpha_channel { 4 } else { 3 }
        };
        for _ in 0..frame_count {
            frames.push(vec![0; frame_size as usize]);
        }
        CBT {
            header,
            frames,
        }
    }

    /// Creates a new CBTv2 with a pre-allocated frame count.
    /// Preferred over new_empty, to save cpu time.
    pub fn new_with_capacity_v2(width: u16, height: u16, frame_count: u16, with_alpha_channel: bool) -> CBT {
        let flags = CBTFlags {
            has_alpha_channel: with_alpha_channel,
        };
        let header = CBTHeader {
            version: 2,
            width,
            height,
            frame_count,
            flags,
        };
        let mut frames = Vec::new();
        let frame_size = header.width * header.height * {
            if header.flags.has_alpha_channel { 2 } else { 1 }
        };
        for _ in 0..frame_count {
            frames.push(vec![0; frame_size as usize]);
        }
        CBT {
            header,
            frames,
        }
    }

    /// Inserts the given data into the frame at the given index.
    pub fn fill_frame(&mut self, frame_index: usize, data: &[u8]) {
        self.frames[frame_index] = data.to_vec();
    }

    /// Converts RGB data into v2 palette indices and inserts it into the frame at the given index.
    /// Parameter `a` should be set to `true` if your data has an alpha channel.
    pub fn fill_v2_with_rgb(&mut self, frame_index: usize, data: &[u8], a: bool) {
        let mut frame = Vec::new();
        let mut i = 0;
        while i < data.len() {
            let r = data[i];
            let g = data[i + 1];
            let b = data[i + 2];
            let index = find_closest_index(r, g, b);
            frame.push(index as u8);
            if a && self.header.flags.has_alpha_channel {
                frame.push(data[i + 3]);
            }
            i += { if a { 4 } else { 3 } };
        }
        self.frames[frame_index] = frame;
    }

    /// Converts v2 palette indices into RGB data and returns a Vec of the data.
    /// Parameter `a` should be set to `true` if you want an alpha channel output
    /// (will output 255 if CBT has no alpha channel).
    pub fn convert_v2_indices_to_rgb(&mut self, frame_index: usize, a: bool) -> Result<Vec<u8>, CBTError> {
        let frame = &self.frames[frame_index];
        let mut data = Vec::new();
        let mut i = 0;
        while i < frame.len() {
            let index = frame[i] as usize;
            if index > palette.len() {
                return Err(CBTError::IndexOutOfBounds);
            }
            let color = palette[index];
            data.push(color.r);
            data.push(color.g);
            data.push(color.b);
            if a && !self.header.flags.has_alpha_channel {
                data.push(255);
            } else if a && self.header.flags.has_alpha_channel {
                data.push(frame[i + 1]);
            }
            if self.header.flags.has_alpha_channel {
                i += 1;
            }
            i += 1;
        }
        Ok(data)
    }

    /// Converts the CBT struct to a byte array that would be in the CBT file format.
    pub fn to_file_data(&self) -> Vec<u8> {
        let header_bytes = self.header.to_bytes();
        let mut data = Vec::new();
        for frame in &self.frames {
            data.extend_from_slice(frame);
        }
        let final_size = header_bytes.len() + data.len();
        let mut final_bytes = Vec::with_capacity(final_size);
        final_bytes.extend_from_slice(&header_bytes);
        final_bytes.extend_from_slice(&data);
        final_bytes
    }

    /// Returns a CBTReader that can be used to read the frames in a specific format.
    /// This should only be used if you're too lazy to do the correct readings yourself,
    /// as it will consume a lot of memory.
    pub fn get_reader(&self, with_alpha_channel: bool) -> CBTReader {
        // if the file has no alpha channel and we don't want one, or vice versa, then we can just return the same CBT
        if self.header.flags.has_alpha_channel == with_alpha_channel {
            let mut frames = Vec::new();
            for frame in &self.frames {
                frames.push(frame.clone());
            }
            return CBTReader { frames };
        }
        // otherwise, we need to convert the frames
        let mut frames = Vec::new();
        match self.header.version {
            1 => match self.header.flags.has_alpha_channel {
                true => {
                for frame in & self.frames {
                let mut new_frame = Vec::new();
                let mut i = 0;
                while i < frame.len() {
                new_frame.push(frame[i]);
                new_frame.push(frame[i + 1]);
                new_frame.push(frame[i + 2]);
                if with_alpha_channel {
                new_frame.push(frame[i + 3]);
                }
                i += 4;
                }
                frames.push(new_frame);
                }
                },
                false => {
                for frame in & self.frames {
                let mut new_frame = Vec::new();
                let mut i = 0;
                while i < frame.len() {
                new_frame.push(frame[i]);
                new_frame.push(frame[i + 1]);
                new_frame.push(frame[i + 2]);
                if with_alpha_channel {
                new_frame.push(255);
                }
                i += 4;
                }
                frames.push(new_frame);
                }
                },
            }
            2 => match self.header.flags.has_alpha_channel {
                true => {
                    for frame in & self.frames {
                        let mut new_frame = Vec::new();
                        let mut i = 0;
                        while i < frame.len() {
                            new_frame.push(frame[i]);
                            if with_alpha_channel {
                                new_frame.push(frame[i + 1]);
                            }
                            i += 2;
                        }
                        frames.push(new_frame);
                    }
                },
                false => {
                    for frame in & self.frames {
                        let mut new_frame = Vec::new();
                        let mut i = 0;
                        while i < frame.len() {
                            new_frame.push(frame[i]);
                            if with_alpha_channel {
                                new_frame.push(255);
                            }
                            i += 1;
                        }
                        frames.push(new_frame);
                    }
                },
            }
            _ => panic!("Unsupported CBT version"),
        }
        CBTReader { frames }
    }
}

impl PartialEq for CBT {
    fn eq(&self, other: &CBT) -> bool {
        self.header == other.header && self.frames == other.frames
    }
}