#![no_std]

extern crate alloc;

pub mod types;
pub mod common;
pub mod quantisation;

// 1 - RGBA
// 2 - INDEXED (uses a constant palette, check colours.h)
static SUPPORTED_VERSIONS: [u8; 2] = [1, 2];
static DEFAULT_VERSION: u8 = 1;

#[cfg(test)]
mod tests {
    use alloc::vec;
    use crate::common::CBT;
    use super::*;

    #[test]
    fn create_two_identical_empty_cbts_v1() {
        let cbt_a = CBT::new_empty_v1(10, 10, false);
        let cbt_b = CBT::new_empty_v1(10, 10, false);
        assert_eq!(cbt_a, cbt_b);
    }

    #[test]
    fn create_two_identical_nonempty_cbts_v1() {
        let mut cbt_a = CBT::new_with_capacity_v1(10, 10, 10, false);
        let mut cbt_b = CBT::new_with_capacity_v1(10, 10, 10, false);
        let data = vec![5; 10 * 10];
        cbt_a.fill_frame(0, &data);
        cbt_b.fill_frame(0, &data);
        assert_eq!(cbt_a, cbt_b);
    }

    #[test]
    fn create_two_different_nonempty_cbts_v1() {
        let mut cbt_a = CBT::new_with_capacity_v1(10, 10, 10, false);
        let mut cbt_b = CBT::new_with_capacity_v1(10, 10, 10, false);
        let data = vec![5; 10 * 10];
        cbt_a.fill_frame(0, &data);
        cbt_b.fill_frame(1, &data);
        assert_ne!(cbt_a, cbt_b);
    }

    #[test]
    fn test_reader_vs_same_data_v1() {
        let cbt_a = CBT::new_with_capacity_v1(10, 10, 10, false);
        let reader = cbt_a.get_reader(false);
        assert_eq!(reader.frames, cbt_a.frames);
    }

    #[test]
    fn test_reader_vs_different_data_v1() {
        let cbt_a = CBT::new_with_capacity_v1(10, 10, 10, false);
        let reader = cbt_a.get_reader(true);
        assert_ne!(reader.frames, cbt_a.frames);
    }

    #[test]
    fn check_length_of_rgb_frame_v1() {
        let cbt_a = CBT::new_with_capacity_v1(10, 10, 10, false);
        let reader = cbt_a.get_reader(false);
        assert_eq!(reader.frames[0].len(), 10 * 10 * 3);
    }

    #[test]
    fn check_length_of_rgba_frame_v1() {
        let cbt_a = CBT::new_with_capacity_v1(10, 10, 10, true);
        let reader = cbt_a.get_reader(true);
        assert_eq!(reader.frames[0].len(), 10 * 10 * 4);
    }

    #[test]
    fn save_and_reopen_v1() {
        let mut cbt = CBT::new_with_capacity_v1(10, 10, 10, false);
        cbt.fill_frame(0, &vec![5; 10 * 10 * 3]);
        let saved_cbt = cbt.to_file_data();
        let mut cbt_intheory_same = CBT::new_with_capacity_v1(10, 10, 10, false);
        cbt_intheory_same.fill_frame(0, &vec![5; 10 * 10 * 3]);
        let reopened_cbt = CBT::new_from_bytes(&saved_cbt).unwrap();
        assert_eq!(reopened_cbt, cbt_intheory_same);
    }

    #[test]
    fn save_and_reopen_rgba_v1() {
        let mut cbt = CBT::new_with_capacity_v1(10, 10, 10, true);
        cbt.fill_frame(0, &vec![5; 10 * 10 * 4]);
        let saved_cbt = cbt.to_file_data();
        let mut cbt_intheory_same = CBT::new_with_capacity_v1(10, 10, 10, true);
        cbt_intheory_same.fill_frame(0, &vec![5; 10 * 10 * 4]);
        let reopened_cbt = CBT::new_from_bytes(&saved_cbt).unwrap();
        assert_eq!(reopened_cbt, cbt_intheory_same);
    }

    #[test]
    fn fill_with_white_v2() {
        let mut cbt = CBT::new_with_capacity_v2(10, 10, 10, false);
        cbt.fill_v2_with_rgb(0, &vec![255; 10 * 10 * 3], false);
        let data = cbt.frames[0].clone();
        assert_eq!(data, vec![136; 10 * 10]);
    }

    #[test]
    fn fill_with_black_v2() {
        let mut cbt = CBT::new_with_capacity_v2(10, 10, 10, false);
        cbt.fill_v2_with_rgb(0, &vec![0; 10 * 10 * 3], false);
        let data = cbt.frames[0].clone();
        assert_eq!(data, vec![7; 10 * 10]);
    }

    #[test]
    fn convert_white_indices_to_rgb_v2() {
        let mut cbt = CBT::new_with_capacity_v2(10, 10, 10, false);
        cbt.fill_frame(0, &[136; 10 * 10]);
        let data = cbt.convert_v2_indices_to_rgb(0, false).unwrap();
        assert_eq!(data, vec![255; 10 * 10 * 3]);
    }

    #[test]
    fn input_output_alpha_v2() {
        let mut cbt = CBT::new_with_capacity_v2(10, 10, 10, true);
        cbt.fill_v2_with_rgb(0, &vec![255; 10 * 10 * 4], true);
        let data = cbt.convert_v2_indices_to_rgb(0, false).unwrap();
        assert_eq!(data, vec![255; 10 * 10 * 3]);
    }
}
