# libCBT
### (the Compact emBedded multiple image animation file formaT)

have you ever wanted to use a GIF animation in something like a bootloader for an operating system that just so
happens to be programmed in rust? have you been annoyed by the fact that the rust gif crate is not no-std compatible?

## i love CBT

#### definition of a v1 .cbt file
```
< HEADER > < u8 Red > < u8 Green > < u8 Blue > (for each pixel)
```
yep, pretty much just that.   
here's the header:
```
CBTSEX < u8 Version (1) > < u16le width > < u16le height > < u16 frame count > < u8 packed settings >
```
the settings are (in little endian):
```
bit 1 = enable alpha channel (a new u8 after each blue value)
```

#### definition of a v2 .cbt file
```
< HEADER > < u8 PSCLR index > (for each pixel)
```
PSCLR refers to the ProtoServe palette, which can be found in the `colour.h` file of this repository.
```
CBTSEX < u8 Version (2) > < u16le width > < u16le height > < u16 frame count > < u8 packed settings >
```
the settings are (in little endian):
```
bit 1 = enable alpha channel (a new u8 after each value)
```
