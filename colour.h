//
// Created by husky on 7/21/22.
//

#ifndef PROTOSRV_COLOUR_H
#define PROTOSRV_COLOUR_H

/*
 * contains definitions to convert web-safe colours into colours for CGA, EGA, and VGA graphics cards.
 * also contains definitions for all the ProtoServe-supported colours in general.
 *
 * if in CGA mode, the colours will be from 0 to 3.
 * if in EGA mode, the colours will be from 0 to 15.
 * if in VGA mode, the colours will be from 0 to 255.
 *
 * on CGA, ProtoServe uses palette 1 (cyan, magenta, white, and black).
 */

#include <stdint.h>

// 0
#define CGA_ALICE_BLUE 3
#define EGA_ALICE_BLUE 15
#define VGA_ALICE_BLUE 100

// 1
#define CGA_ANTIQUE_WHITE 3
#define EGA_ANTIQUE_WHITE 15
#define VGA_ANTIQUE_WHITE 90


// 2
#define CGA_AQUA 1
#define EGA_AQUA 11
#define VGA_AQUA 52


// 3
#define CGA_AQUAMARINE 1
#define EGA_AQUAMARINE 10
#define VGA_AQUAMARINE 51

// 4
#define CGA_AZURE 3
#define EGA_AZURE 15
#define VGA_AZURE 101

// 5
#define CGA_BEIGE 3
#define EGA_BEIGE 15
#define VGA_BEIGE 91

// 6
#define CGA_BISQUE 3
#define EGA_BISQUE 15
#define VGA_BISQUE 90

// 7
#define CGA_BLACK 0
#define EGA_BLACK 0
#define VGA_BLACK 0


// 8
#define CGA_BLANCHED_ALMOND 3
#define EGA_BLANCHED_ALMOND 15
#define VGA_BLANCHED_ALMOND 92

// 9
#define CGA_BLUE 1
#define EGA_BLUE 1
#define VGA_BLUE 32

// 10
#define CGA_BLUE_VIOLET 2
#define EGA_BLUE_VIOLET 13
#define VGA_BLUE_VIOLET 34

// 11
#define CGA_BROWN 2
#define EGA_BROWN 12
#define VGA_BROWN 41

// 12
#define CGA_BURLY_WOOD 3
#define EGA_BURLY_WOOD 15
#define VGA_BURLY_WOOD 66

// 13
#define CGA_CADET_BLUE 1
#define EGA_CADET_BLUE 9
#define VGA_CADET_BLUE 78

// 14
#define CGA_CHARTREUSE 1
#define EGA_CHARTREUSE 10
#define VGA_CHARTREUSE 48

// 15
#define CGA_CHOCOLATE 2
#define EGA_CHOCOLATE 6
#define VGA_CHOCOLATE 42

// 16
#define CGA_CORAL 2
#define EGA_CORAL 12
#define VGA_CORAL 64

// 17
#define CGA_CORNFLOWER_BLUE 1
#define EGA_CORNFLOWER_BLUE 9
#define VGA_CORNFLOWER_BLUE 78

// 18
#define CGA_CORNSILK 3
#define EGA_CORNSILK 15
#define VGA_CORNSILK 92

// 19
#define CGA_CRIMSON 2
#define EGA_CRIMSON 12
#define VGA_CRIMSON 39

// 20
#define CGA_CYAN 1
#define EGA_CYAN 11
#define VGA_CYAN 52

// 21
#define CGA_DARK_BLUE 0
#define EGA_DARK_BLUE 1
#define VGA_DARK_BLUE 1

// 22
#define CGA_DARK_CYAN 0
#define EGA_DARK_CYAN 3
#define VGA_DARK_CYAN 3

// 23
#define CGA_DARK_GOLDENROD 0
#define EGA_DARK_GOLDENROD 6
#define VGA_DARK_GOLDENROD 43


// 24
#define CGA_DARK_GREY 0
#define EGA_DARK_GREY 8
#define VGA_DARK_GREY 29

// 25
#define CGA_DARK_GREEN 0
#define EGA_DARK_GREEN 2
#define VGA_DARK_GREEN 120

// 26
#define CGA_DARK_KHAKI 3
#define EGA_DARK_KHAKI 6
#define VGA_DARK_KHAKI 139

// 27
#define CGA_DARK_MAGENTA 0
#define EGA_DARK_MAGENTA 5
#define VGA_DARK_MAGENTA 5

// 28
#define CGA_DARK_OLIVE_GREEN 3
#define EGA_DARK_OLIVE_GREEN 2
#define VGA_DARK_OLIVE_GREEN 117

// 29
#define CGA_DARK_ORANGE 2
#define EGA_DARK_ORANGE 14
#define VGA_DARK_ORANGE 42

// 30
#define CGA_DARK_RED 0
#define EGA_DARK_RED 4
#define VGA_DARK_RED 4

// 31
#define CGA_DARK_SALMON 3
#define EGA_DARK_SALMON 12
#define VGA_DARK_SALMON 64

// 32
#define CGA_DARK_SEA_GREEN 1
#define EGA_DARK_SEA_GREEN 10
#define VGA_DARK_SEA_GREEN 169

// 33
#define CGA_DARK_SLATE_BLUE 0
#define EGA_DARK_SLATE_BLUE 3
#define VGA_DARK_SLATE_BLUE 32

// 34
#define CGA_DARK_SLATE_GREY 0
#define EGA_DARK_SLATE_GREY 8
#define VGA_DARK_SLATE_GREY 221

// 35
#define CGA_DARK_TURQUOISE 1
#define EGA_DARK_TURQUOISE 3
#define VGA_DARK_TURQUOISE 3

// 36
#define CGA_DARK_VIOLET 2
#define EGA_DARK_VIOLET 5
#define VGA_DARK_VIOLET 34

// 37
#define CGA_DEEP_PINK 2
#define EGA_DEEP_PINK 13
#define VGA_DEEP_PINK 37

// 38
#define CGA_DEEP_SKY_BLUE 1
#define EGA_DEEP_SKY_BLUE 11
#define VGA_DEEP_SKY_BLUE 53

// 39
#define CGA_DIM_GREY 0
#define EGA_DIM_GREY 8
#define VGA_DIM_GREY 21

// 40
#define CGA_DODGER_BLUE 1
#define EGA_DODGER_BLUE 3
#define VGA_DODGER_BLUE 54

// 41
#define CGA_FIREBRICK 2
#define EGA_FIREBRICK 4
#define VGA_FIREBRICK 4

// 42
#define CGA_FLORAL_WHITE 3
#define EGA_FLORAL_WHITE 15
#define VGA_FLORAL_WHITE 30

// 43
#define CGA_FOREST_GREEN 1
#define EGA_FOREST_GREEN 2
#define VGA_FOREST_GREEN 120

// 44
#define CGA_FUCHSIA 2
#define EGA_FUCHSIA 13
#define VGA_FUCHSIA 36

// 45
#define CGA_GAINSBORO 3
#define EGA_GAINSBORO 7
#define VGA_GAINSBORO 29

// 46
#define CGA_GHOST_WHITE 3
#define EGA_GHOST_WHITE 15
#define VGA_GHOST_WHITE 31

// 47
#define CGA_GOLD 2
#define EGA_GOLD 14
#define VGA_GOLD 43

// 48
#define CGA_GOLDENROD 2
#define EGA_GOLDENROD 6
#define VGA_GOLDENROD 42

// 49
#define CGA_GREY 0
#define EGA_GREY 8
#define VGA_GREY 23

// 50
#define CGA_GREEN 1
#define EGA_GREEN 2
#define VGA_GREEN 120

// 51
#define CGA_GREEN_YELLOW 1
#define EGA_GREEN_YELLOW 10
#define VGA_GREEN_YELLOW 45

// 52
#define CGA_HONEYDEW 3
#define EGA_HONEYDEW 15
#define VGA_HONEYDEW 30

// 53
#define CGA_HOT_PINK 2
#define EGA_HOT_PINK 13
#define VGA_HOT_PINK 36

// 54
#define CGA_INDIAN_RED 2
#define EGA_INDIAN_RED 4
#define VGA_INDIAN_RED 136

// 55
#define CGA_INDIGO 0
#define EGA_INDIGO 1
#define VGA_INDIGO 106

// 56
#define CGA_IVORY 3
#define EGA_IVORY 15
#define VGA_IVORY 92

// 57
#define CGA_KHAKI 3
#define EGA_KHAKI 14
#define VGA_KHAKI 139

// 58
#define CGA_LAVENDER 3
#define EGA_LAVENDER 7
#define VGA_LAVENDER 80

// 59
#define CGA_LAVENDER_BLUSH 3
#define EGA_LAVENDER_BLUSH 15
#define VGA_LAVENDER_BLUSH 86

// 60
#define CGA_LAWN_GREEN 1
#define EGA_LAWN_GREEN 10
#define VGA_LAWN_GREEN 47

// 61
#define CGA_LEMON_CHIFFON 3
#define EGA_LEMON_CHIFFON 15
#define VGA_LEMON_CHIFFON 90

// 62
#define CGA_LIGHT_BLUE 1
#define EGA_LIGHT_BLUE 11
#define VGA_LIGHT_BLUE 77

// 63
#define CGA_LIGHT_CORAL 2
#define EGA_LIGHT_CORAL 12
#define VGA_LIGHT_CORAL 64

// 64
#define CGA_LIGHT_CYAN 3
#define EGA_LIGHT_CYAN 15
#define VGA_LIGHT_CYAN 112

// 65
#define CGA_LIGHT_GOLDENROD_YELLOW 3
#define EGA_LIGHT_GOLDENROD_YELLOW 15
#define VGA_LIGHT_GOLDENROD_YELLOW 92

// 66
#define CGA_LIGHT_GREY 3
#define EGA_LIGHT_GREY 7
#define VGA_LIGHT_GREY 30

// 67
#define CGA_LIGHT_GREEN 1
#define EGA_LIGHT_GREEN 10
#define VGA_LIGHT_GREEN 50

// 68
#define CGA_LIGHT_PINK 3
#define EGA_LIGHT_PINK 15
#define VGA_LIGHT_PINK 89

// 69
#define CGA_LIGHT_SALMON 2
#define EGA_LIGHT_SALMON 12
#define VGA_LIGHT_SALMON 65

// 70
#define CGA_LIGHT_SEA_GREEN 1
#define EGA_LIGHT_SEA_GREEN 11
#define VGA_LIGHT_SEA_GREEN 147

// 71
#define CGA_LIGHT_SKY_BLUE 1
#define EGA_LIGHT_SKY_BLUE 11
#define VGA_LIGHT_SKY_BLUE 76

// 72
#define CGA_LIGHT_SLATE_GREY 0
#define EGA_LIGHT_SLATE_GREY 8
#define VGA_LIGHT_SLATE_GREY 223

// 73
#define CGA_LIGHT_STEEL_BLUE 1
#define EGA_LIGHT_STEEL_BLUE 7
#define VGA_LIGHT_STEEL_BLUE 103

// 74
#define CGA_LIGHT_YELLOW 3
#define EGA_LIGHT_YELLOW 15
#define VGA_LIGHT_YELLOW 92

// 75
#define CGA_LIME 1
#define EGA_LIME 10
#define VGA_LIME 48

// 76
#define CGA_LIME_GREEN 1
#define EGA_LIME_GREEN 2
#define VGA_LIME_GREEN 121

// 77
#define CGA_LINEN 3
#define EGA_LINEN 15
#define VGA_LINEN 89

// 78
#define CGA_MAGENTA 2
#define EGA_MAGENTA 13
#define VGA_MAGENTA 36

// 79
#define CGA_MAROON 0
#define EGA_MAROON 4
#define VGA_MAROON 4

// 80
#define CGA_MEDIUM_AQUAMARINE 1
#define EGA_MEDIUM_AQUAMARINE 11
#define VGA_MEDIUM_AQUAMARINE 67

// 81
#define CGA_MEDIUM_BLUE 0
#define EGA_MEDIUM_BLUE 1
#define VGA_MEDIUM_BLUE 32

// 82
#define CGA_MEDIUM_ORCHID 2
#define EGA_MEDIUM_ORCHID 5
#define VGA_MEDIUM_ORCHID 132

// 83
#define CGA_MEDIUM_PURPLE 2
#define EGA_MEDIUM_PURPLE 5
#define VGA_MEDIUM_PURPLE 130

// 84
#define CGA_MEDIUM_SEA_GREEN 1
#define EGA_MEDIUM_SEA_GREEN 10
#define VGA_MEDIUM_SEA_GREEN 144

// 85
#define CGA_MEDIUM_SLATE_BLUE 1
#define EGA_MEDIUM_SLATE_BLUE 7
#define VGA_MEDIUM_SLATE_BLUE 129

// 86
#define CGA_MEDIUM_SPRING_GREEN 1
#define EGA_MEDIUM_SPRING_GREEN 10
#define VGA_MEDIUM_SPRING_GREEN 50

// 87
#define CGA_MEDIUM_TURQUOISE 1
#define EGA_MEDIUM_TURQUOISE 3
#define VGA_MEDIUM_TURQUOISE 3

// 88
#define CGA_MEDIUM_VIOLET_RED 2
#define EGA_MEDIUM_VIOLET_RED 5
#define VGA_MEDIUM_VIOLET_RED 132

// 89
#define CGA_MIDNIGHT_BLUE 0
#define EGA_MIDNIGHT_BLUE 1
#define VGA_MIDNIGHT_BLUE 114

// 90
#define CGA_MINT_CREAM 3
#define EGA_MINT_CREAM 15
#define VGA_MINT_CREAM 31

// 91
#define CGA_MISTY_ROSE 3
#define EGA_MISTY_ROSE 15
#define VGA_MISTY_ROSE 105

// 92
#define CGA_MOCCASIN 3
#define EGA_MOCCASIN 15
#define VGA_MOCCASIN 67

// 93
#define CGA_NAVAJO_WHITE 3
#define EGA_NAVAJO_WHITE 15
#define VGA_NAVAJO_WHITE 66

// 94
#define CGA_NAVY 0
#define EGA_NAVY 1
#define VGA_NAVY 115

// 95
#define CGA_OLD_LACE 3
#define EGA_OLD_LACE 15
#define VGA_OLD_LACE 91

// 96
#define CGA_OLIVE 0
#define EGA_OLIVE 2
#define VGA_OLIVE 190

// 97
#define CGA_OLIVE_DRAB 0
#define EGA_OLIVE_DRAB 2
#define VGA_OLIVE_DRAB 189

// 98
#define CGA_ORANGE 2
#define EGA_ORANGE 14
#define VGA_ORANGE 43

// 99
#define CGA_ORANGE_RED 2
#define EGA_ORANGE_RED 12
#define VGA_ORANGE_RED 41

// 100
#define CGA_ORCHID 2
#define EGA_ORCHID 13
#define VGA_ORCHID 61

// 101
#define CGA_PALE_GOLDENROD 3
#define EGA_PALE_GOLDENROD 15
#define VGA_PALE_GOLDENROD 68

// 102
#define CGA_PALE_GREEN 1
#define EGA_PALE_GREEN 10
#define VGA_PALE_GREEN 72

// 103
#define CGA_PALE_TURQUOISE 1
#define EGA_PALE_TURQUOISE 11
#define VGA_PALE_TURQUOISE 74

// 104
#define CGA_PALE_VIOLET_RED 2
#define EGA_PALE_VIOLET_RED 5
#define VGA_PALE_VIOLET_RED 63

// 105
#define CGA_PAPAYA_WHIP 3
#define EGA_PAPAYA_WHIP 15
#define VGA_PAPAYA_WHIP 90

// 106
#define CGA_PEACH_PUFF 3
#define EGA_PEACH_PUFF 15
#define VGA_PEACH_PUFF 89

// 107
#define CGA_PERU 0
#define EGA_PERU 6
#define VGA_PERU 44

// 108
#define CGA_PINK 2
#define EGA_PINK 13
#define VGA_PINK 87

// 109
#define CGA_PLUM 2
#define EGA_PLUM 13
#define VGA_PLUM 60

// 110
#define CGA_POWDER_BLUE 1
#define EGA_POWDER_BLUE 11
#define VGA_POWDER_BLUE 75

// 111
#define CGA_PURPLE 2
#define EGA_PURPLE 5
#define VGA_PURPLE 5

// 112
#define CGA_REBECCA_PURPLE 2
#define EGA_REBECCA_PURPLE 5
#define VGA_REBECCA_PURPLE 34

// 113
#define CGA_RED 2
#define EGA_RED 12
#define VGA_RED 40

// 114
#define CGA_ROSY_BROWN 2
#define EGA_ROSY_BROWN 5
#define VGA_ROSY_BROWN 66

// 115
#define CGA_ROYAL_BLUE 1
#define EGA_ROYAL_BLUE 9
#define VGA_ROYAL_BLUE 9

// 116
#define CGA_SADDLE_BROWN 0
#define EGA_SADDLE_BROWN 6
#define VGA_SADDLE_BROWN 6

// 117
#define CGA_SALMON 2
#define EGA_SALMON 12
#define VGA_SALMON 64

// 118
#define CGA_SANDY_BROWN 3
#define EGA_SANDY_BROWN 14
#define VGA_SANDY_BROWN 65

// 119
#define CGA_SEA_GREEN 1
#define EGA_SEA_GREEN 2
#define VGA_SEA_GREEN 192

// 120
#define CGA_SEA_SHELL 3
#define EGA_SEA_SHELL 15
#define VGA_SEA_SHELL 88

// 121
#define CGA_SIENNA 0
#define EGA_SIENNA 6
#define VGA_SIENNA 43

// 122
#define CGA_SILVER 1
#define EGA_SILVER 7
#define VGA_SILVER 30

// 123
#define CGA_SKY_BLUE 1
#define EGA_SKY_BLUE 11
#define VGA_SKY_BLUE 78

// 124
#define CGA_SLATE_BLUE 1
#define EGA_SLATE_BLUE 7
#define VGA_SLATE_BLUE 129

// 125
#define CGA_SLATE_GREY 1
#define EGA_SLATE_GREY 8
#define VGA_SLATE_GREY 130

// 126
#define CGA_SNOW 3
#define EGA_SNOW 15
#define VGA_SNOW 31

// 127
#define CGA_SPRING_GREEN 1
#define EGA_SPRING_GREEN 10
#define VGA_SPRING_GREEN 50

// 128
#define CGA_STEEL_BLUE 1
#define EGA_STEEL_BLUE 9
#define VGA_STEEL_BLUE 124

// 129
#define CGA_TAN 3
#define EGA_TAN 14
#define VGA_TAN 67

// 130
#define CGA_TEAL 0
#define EGA_TEAL 2
#define VGA_TEAL 194

// 131
#define CGA_THISTLE 3
#define EGA_THISTLE 15
#define VGA_THISTLE 85

// 132
#define CGA_TOMATO 2
#define EGA_TOMATO 12
#define VGA_TOMATO 64

// 133
#define CGA_TURQUOISE 1
#define EGA_TURQUOISE 11
#define VGA_TURQUOISE 76

// 134
#define CGA_VIOLET 2
#define EGA_VIOLET 13
#define VGA_VIOLET 61

// 135
#define CGA_WHEAT 3
#define EGA_WHEAT 15
#define VGA_WHEAT 91

// 136
#define CGA_WHITE 3
#define EGA_WHITE 15
#define VGA_WHITE 15

// 137
#define CGA_WHITE_SMOKE 3
#define EGA_WHITE_SMOKE 15
#define VGA_WHITE_SMOKE 30

// 138
#define CGA_YELLOW 1
#define EGA_YELLOW 14
#define VGA_YELLOW 14

// 139
#define CGA_YELLOW_GREEN 1
#define EGA_YELLOW_GREEN 10
#define VGA_YELLOW_GREEN 50

void init_colour_cga();
void init_colour_ega();

#endif //PROTOSRV_COLOUR_H
